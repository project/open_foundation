[![](https://www.drupal.org/files/styles/grid-4-2x/public/OSL%20Logo%202020-01.png?itok=0Yve8wuf)](https://www.drupal.org/project/open_foundation)


## Open Foundation - A Drupal pre-built package with essential modules and a theme.

## Installation Instructions

## Create a Project
For Drupal 10:
composer create-project drupal/recommended-project my_site_name

## Add Openbase to Your Project

After creating your project, download the OpenBase project.
```
Move the OpenBase project to your Drupal project's web/profiles directory.
```
Replace the existing composer.json and composer.lock files in the root directory with the ones provided in the OpenBase folder.
```
Run the following command to install the necessary packages:
composer install
```

## Install Drupal Using the Standard Web Interface

1. After Composer has downloaded the packages, navigate to your site's URL in your browser to begin the setup process.
```
2. Select the "OpenBase" profile from the available options and proceed with the installation.
```
3. You will be prompted to enter your database credentials, create an admin user, and provide some basic information about your site.
```
4. That's it! Your Drupal site with OpenBase should now be set up and ready to use.
```

